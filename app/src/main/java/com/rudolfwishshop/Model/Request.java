package com.rudolfwishshop.Model;

import java.util.List;

public class Request {


    private String address;
    private String total;
    private String status;

    private List<Order> products;

    public Request() {
    }

    public Request( String address, String total, String status,  List<Order> products) {

        this.address = address;
        this.total = total;
        this.status = status;

        this.products = products;
    }




    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }




    public List<Order> getProducts() {
        return products;
    }

    public void setProducts(List<Order> products) {
        this.products = products;
    }
}
