package com.rudolfwishshop.Model;


public class Product {

    private String Name;
    private String Image;
    private String Description;
    private String  discount;
    private String Price;
    private String MenuId;


    public Product() {
    }

    public Product(String name, String image, String description, String discount, String price, String menuId) {
        this.Name = name;
        this.Image = image;
        this.Description = description;
        this.discount = discount;
        this.Price = price;
        MenuId = menuId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        this.Image = image;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        this.Price = price;
    }

    public String getMenuId() {
        return MenuId;
    }

    public void setMenuId(String menuId) {
        MenuId = menuId;
    }
}
