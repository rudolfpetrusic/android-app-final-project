package com.rudolfwishshop.ViewHolder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.rudolfwishshop.Model.Order;
import com.rudolfwishshop.R;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView cartProductName;
    public TextView cartProductPrice;

    public CartViewHolder(@NonNull View itemView) {
        super(itemView);

        cartProductName = (TextView)itemView.findViewById(R.id.cart_item_name);
        cartProductPrice = (TextView)itemView.findViewById(R.id.cart_item_price);

    }

    @Override
    public void onClick(View v) {

    }
}

