package com.rudolfwishshop;

import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rudolfwishshop.Database.Database;
import com.rudolfwishshop.Model.Order;
import com.rudolfwishshop.Model.Product;
import com.squareup.picasso.Picasso;

public class ProductDetails extends AppCompatActivity {

    TextView productName, product_price, product_desc;
    ImageView product_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnCart;

    String productId= "";
    FirebaseDatabase database;
    DatabaseReference product;

    Product currentProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);


        //Firebase
        database = FirebaseDatabase.getInstance();
        product = database.getReference("Products");


        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        productName = (TextView)findViewById(R.id.product_name);
        product_price = (TextView)findViewById(R.id.product_price);
        product_desc = (TextView) findViewById(R.id.product_desc);

        product_image = (ImageView) findViewById(R.id.img_product);
        btnCart = (FloatingActionButton)findViewById(R.id.btnCart);

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Database(getBaseContext()).addToCart(new Order(
                        productId,
                        currentProduct.getName(),
                        currentProduct.getPrice()
                ));

                Toast.makeText(ProductDetails.this, "Dodali ste novi proizvod u korpu", Toast.LENGTH_SHORT).show();
            }
        });
        //Get Product id from intent
        if(getIntent() != null)
            productId = getIntent().getStringExtra("ProductId");
        if(!productId.isEmpty()){
            getDetailProduct(productId);
        }
    }

    private void getDetailProduct(final String productId){
        product.child(productId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentProduct = dataSnapshot.getValue(Product.class);

                //Set Image
                Picasso.get().load(currentProduct.getImage())
                        .into(product_image);



                    collapsingToolbarLayout.setTitle(currentProduct.getName());



                product_price.setText(currentProduct.getPrice());
                productName.setText(currentProduct.getName());
                product_desc.setText(currentProduct.getDescription());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }



}
