package com.rudolfwishshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;

public class MainActivity extends AppCompatActivity {

    Button btnRegistration;
    Button btnSignIN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRegistration = (Button) findViewById(R.id.registrationBtn);
        btnSignIN = (Button)findViewById(R.id.signInBtn);

        FirebaseApp.initializeApp(this);
        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regiAc = new Intent(MainActivity.this, Registration.class);
                startActivity(regiAc);
                //TO DO ADD Finish method
            }
        });

        btnSignIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signAc = new Intent(MainActivity.this, SignIn.class);
                startActivity(signAc);
                //TO DO ADD Finish method
            }
        });
    }
}
